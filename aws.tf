# aws.tf
variable "AWS_ACCESS_KEY" {}

variable "AWS_SECRET_KEY" {}

provider "aws" {
  region = "ap-south-1"
  access_key = "${var.AWS_ACCESS_KEY}"
  secret_key = "${var.AWS_SECRET_KEY}"
}

resource "aws_key_pair" "gokul-key" {
  key_name = "gokul-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCF/p2hKKIhpR83lPlybOgwE04DUMhowTf/K2qPrYpgow4AmoWNU84iN8GNm0uhg63YfxdBIJsMz226nEmgbWUaE8RwZlmlgYalipAEqWqyTYLAtvPsAhfDwQgizScAV7HB3ghK7svccjdLr91/Ey9OIxglxOZIyVqHrx6Jo7BCVzctOvJRTt7gkOdVPa6NeZ/xPVKn6Qf3VgNOZ4RRCxI9Z5EbcgCN4StkZJNvAb+Mt6tYkqmUjtMWEaKjyeC5jXd5IXaKCtgmcpwqNTbleSv3DEk3dZrm726NDB/mhKoiP6lu8INV0sUKtTeq9iiqOv3tF/qNA1EiS4qMd0bCBE// tutorial"
}

resource "aws_instance" "web" {
    ami = "ami-2b95a744" 
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.gokul-key.key_name}"
    tags {
    Name = "Terraform"
    }
}
